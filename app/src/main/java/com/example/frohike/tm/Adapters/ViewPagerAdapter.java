package com.example.frohike.tm.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.frohike.tm.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Frohike on 02.11.2017.
 */

public class ViewPagerAdapter extends PagerAdapter {

    Context context;
    int count = 3;
    String image1;
    String image2;
    String image3;

    public ViewPagerAdapter(Context context) {
        this.context = context;
    }

    public ViewPagerAdapter(Context context, String image1, String image2, String image3) {
        this.context = context;
        this.image1 = image1;
        this.image2 = image2;
        this.image3 = image3;

    }

    @Override
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        if(count<10){
            this.count = count;
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(imageView, 0);
        switch (position) {

            case 0:
                Picasso.with(context).load(image1)
                        .error(R.mipmap.ic_launcher)
                        .into(imageView);
                break;
            case 1:
                Picasso.with(context).load(image2)
                        .error(R.mipmap.ic_launcher)
                        .into(imageView);
                break;
            case 2:
                Picasso.with(context).load(image3)
                        .error(R.mipmap.ic_launcher)
                        .into(imageView);
        }

        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ((ViewPager) container).removeView((ImageView) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }
}
