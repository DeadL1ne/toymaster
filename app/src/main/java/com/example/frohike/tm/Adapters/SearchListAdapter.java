package com.example.frohike.tm.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.frohike.tm.Entity.Product;
import com.example.frohike.tm.ProductActivity;
import com.example.frohike.tm.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.support.v4.content.ContextCompat.startActivity;
import static com.example.frohike.tm.R.styleable.RecyclerView;

/**
 * Created by Frohike on 13.11.2017.
 */

public class SearchListAdapter extends android.support.v7.widget.RecyclerView.Adapter<SearchListAdapter.ProductViewHolder> {

    List<Product> productList;
    Context context;

    public SearchListAdapter(List<Product> productList, Context context){
        this.productList = productList;
        this.context = context;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        ProductViewHolder pvh = new ProductViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(ProductViewHolder productViewHolder, final int position) {
        productViewHolder.productName.setText(productList.get(position).getName());
        productViewHolder.productPrice.setText(Float.toString(productList.get(position).getPrice()) + "\u20BD");
        /*Picasso.with(context).load(productList.get(position).getImage_1())
                .error(R.mipmap.ic_launcher)
                .into(productViewHolder.productPhoto);*/

        productViewHolder.itemView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intentProduct = new Intent(context, ProductActivity.class);
                intentProduct.putExtra("id", productList.get(position).getId());
                context.startActivity(intentProduct);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView productName;
        TextView productPrice;
        ImageView productPhoto;

        ProductViewHolder(View itemView) {
            super(itemView);
            productName = (TextView) itemView.findViewById(R.id.textItemName);
            productPrice = (TextView) itemView.findViewById(R.id.textItemPrice);
           // productPhoto = (ImageView) itemView.findViewById(R.id.imageItemPhoto);
        }
    }
}


