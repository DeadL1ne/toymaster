package com.example.frohike.tm;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.frohike.tm.Adapters.ViewPagerAdapter;
import com.example.frohike.tm.Entity.Product;
import com.example.frohike.tm.Interfaces.ToyMasterProductApi;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    //TextView textView;
    SearchView searchView;

    //static final int PAGE_COUNT = 3;

    ViewPager pager;
    PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Слайдер картинок
        String img1 = "https://psv4.userapi.com/c816331/u84624418/docs/b9a0d4a21ba1/lex.png?extra=L4NkW-nMDYQJzdXInNswhl5C2WCPPeBoNSGY6Mz0B1HGQM1sLtKoMR09EGYNfeJa5nwvUYwTKMFMThoMS6EqR-5Gh1XMMFto98XFo9xZ7Z5TOY19dPmzjEB14C5VyHN2XK2thBDBoA";
        String img2 = "https://psv4.userapi.com/c816331/u84624418/docs/b9a0d4a21ba1/lex.png?extra=L4NkW-nMDYQJzdXInNswhl5C2WCPPeBoNSGY6Mz0B1HGQM1sLtKoMR09EGYNfeJa5nwvUYwTKMFMThoMS6EqR-5Gh1XMMFto98XFo9xZ7Z5TOY19dPmzjEB14C5VyHN2XK2thBDBoA";
        String img3 = "https://psv4.userapi.com/c816331/u84624418/docs/b9a0d4a21ba1/lex.png?extra=L4NkW-nMDYQJzdXInNswhl5C2WCPPeBoNSGY6Mz0B1HGQM1sLtKoMR09EGYNfeJa5nwvUYwTKMFMThoMS6EqR-5Gh1XMMFto98XFo9xZ7Z5TOY19dPmzjEB14C5VyHN2XK2thBDBoA";
        pager = (ViewPager) findViewById(R.id.viewTopSale);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this, img1, img2, img3);
        pager.setAdapter(viewPagerAdapter);
        CirclePageIndicator circleIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        circleIndicator.setViewPager(pager);
        /*final List<Product> product = new ArrayList<>();
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://94.250.255.191:8090/").addConverterFactory(GsonConverterFactory.create()).build();
        ToyMasterProductApi productApi = retrofit.create(ToyMasterProductApi.class);
        productApi.getProduct("1").enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                product.add(response.body());
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
            }
        });*/
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtra("productName", query);
        startActivity(intent);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_main) {
            // Handle the camera action
        } else if (id == R.id.nav_orders) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_catalog){
            Intent intent = new Intent(this, CatalogActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_basket) {

        } else if (id == R.id.nav_settings) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
