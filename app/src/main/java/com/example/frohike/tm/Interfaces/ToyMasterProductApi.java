package com.example.frohike.tm.Interfaces;

import com.example.frohike.tm.Entity.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Frohike on 11.11.2017.
 */

public interface ToyMasterProductApi {

    @GET("product/{id}")
    Call<Product> getProduct(@Path("id") String id);

    @GET("product/products")
    Call<List<Product>> getProducts();

    @GET("product/getProductByName/{name}")
    Call<List<Product>> getProductByName(@Path("name") String name);

    @POST("product/new")
    void setProduct (@Body Product product);
}
