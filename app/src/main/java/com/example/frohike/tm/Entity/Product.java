package com.example.frohike.tm.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.sql.Timestamp;

import retrofit2.Response;


/**
 * Created by Frohike on 11.11.2017.
 */

public class Product {
    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private float price;
    @SerializedName("category")
    @Expose
    private Long category;
    @SerializedName("lower_age")
    @Expose
    private int lower_age;
    @SerializedName("upper_age")
    @Expose
    private int upper_age;
    @SerializedName("image_1")
    @Expose
    private Long image_1;
    @SerializedName("image_2")
    @Expose
    private Long image_2;
    @SerializedName("image_3")
    @Expose
    private Long image_3;
    @SerializedName("freecount")
    @Expose
    private int freecount;
    @SerializedName("blockcount")
    @Expose
    private int blockcount;
    @SerializedName("salescount")
    @Expose
    private int salescount;
    @SerializedName("create_date")
    @Expose
    private Long create_date;
    @SerializedName("update_date")
    @Expose
    private Long update_date;

    public Product(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }

    public int getLower_age() {
        return lower_age;
    }

    public void setLower_age(int lower_age) {
        this.lower_age = lower_age;
    }

    public int getUpper_age() {
        return upper_age;
    }

    public void setUpper_age(int upper_age) {
        this.upper_age = upper_age;
    }

    public Long getImage_1() {
        return image_1;
    }

    public void setImage_1(Long image_1) {
        this.image_1 = image_1;
    }

    public Long getImage_2() {
        return image_2;
    }

    public void setImage_2(Long image_2) {
        this.image_2 = image_2;
    }

    public Long getImage_3() {
        return image_3;
    }

    public void setImage_3(Long image_3) {
        this.image_3 = image_3;
    }

    public int getFreecount() {
        return freecount;
    }

    public void setFreecount(int freecount) {
        this.freecount = freecount;
    }

    public int getBlockcount() {
        return blockcount;
    }

    public void setBlockcount(int blockcount) {
        this.blockcount = blockcount;
    }

    public int getSalescount() {
        return salescount;
    }

    public void setSalescount(int salescount) {
        this.salescount = salescount;
    }

    public Long getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Long create_date) {
        this.create_date = create_date;
    }

    public Long getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Long update_date) {
        this.update_date = update_date;
    }


}
